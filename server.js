var express = require('express'),
		http = require('http'),
		routes = require('./routes'),
		path = require('path');

var app = express();
var server = http.createServer(app);

app.use(express.bodyParser())
	.use(express.static(path.join(__dirname, 'public')))
	.use(express.cookieParser('Je suis secret !'))
	.use(express.session({secret: 'Ma cle secrete !'}))
	.use(app.router)
	.set('views', __dirname + '/views')
	.set('view engine', 'jade')
	.get('/', routes.index)
	.get('/auth', routes.auth)
	.get('/logout', routes.logout)
	.get('/room/:id', routes.room)
	.get('/profile', routes.profile)
	.get('*', function(req, res){
		res.send(404);
	})
	.post('/register', routes.postRegister)
	.post('/login', routes.postLogin)
	.post('/room/add', routes.createRoom);

	require('./routes/socket')(server)

server.listen(80);
console.log('Listening on port 80');

exports = module.exports = app

