var User = require('../models/User').User,
    users = require('../models/User'),
    Language = require('../models/Language').Language,
    languages = require('../models/Language'),
    Room = require('../models/Room').Room,
    rooms = require('../models/Room'),
    bcrypt     = require('bcrypt-nodejs');

exports.index = function(req, res){
    if (req.session.user){
        Language.find(function (err, languages){
            if(err) {res.send({'error':'An error has occured'}); }
            else {
                res.render('index', {user: req.session.user, languages: languages});
            }
        });
    }else{
        res.redirect('auth');
    }
};

exports.auth = function(req, res){
    if (req.session.user){
        console.log(req.session.user);
        res.redirect('index');
    }else{
        res.render('auth');
    }
};

exports.logout = function(req, res){
    req.session.destroy(function (err) {});
    res.redirect('back');
};

exports.postRegister = function(req, res){
	user = new User({
        username : req.body.username,
        password : bcrypt.hashSync(req.body.password),
        email : req.body.email
    });
	user.save( function(err, r) {
        if(err) {console.log(err);}
        else {
            var time = 3 * 60 * 60 * 1000 //3h
            req.session.user = r;
            res.redirect('/');
        }
  });
};

exports.postLogin = function(req, res){
    var email = req.body.email;
    var pass = req.body.password;
    User.findOne({'email':email}, function(e,r){
        if(r != null){
            if(bcrypt.compareSync(pass, r.password)){
                console.log('Auth OK');
                var time = 3 * 60 * 60 * 1000 //3h
                req.session.user = r;
                res.redirect('/#connect');
            } else {
                console.log('Auth err');
                res.redirect('auth');
            }
        }
        else
            res.redirect('auth');
    });
};

exports.createRoom = function(req, res){
    res.redirect('/room/');
};

exports.profile = function(req ,res){
    if(req.session.user){
        res.render('profile', {user: req.session.user});
    }else {
        res.redirect('/');
    }
}

exports.room = function(req, res){
    if (req.session.user){
        var id = req.params.id;
        console.log(" PARAM ID : "+id);
    }else{
        res.redirect('auth');
    }    

};