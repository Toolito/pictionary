var User = require('../models/User').User,
    users = require('../models/User'),
    Room = require('../models/Room').RoomObj,
    _ = require('underscore')._,
    uuid = require('node-uuid');

module.exports = function(server) {

  var io = require('socket.io').listen(server);
  var rooms = {};
	var users = {};
	var sockets = [];
	var lines = [];
	var chatHistory = {};

	function game(sock, action){
		if(users[sock.id].inRoom){
			var room = rooms[users[sock.id].inRoom];
			if(sock.id === room.owner){
				if(action === "disconnect"){
					io.sockets.in(sock.room).emit("updateUsers", users[sock.id]);
					var socketIds = [];
					for (var i=0; i<sockets.length; i++){
						socketIds.push(sockets[i].id);
						if(_.contains((socketIds)), room.users){
							sockets[i].leave(room.name);
						}
					}

					if(_.contains((room.users)), sock.id){
						for (var i = 0; i<room.users.length; i++){
							users[room.users[i]].inRoom = null;
						}
					}
					room.users = _.without(room.users, sock.id);
					delete rooms[users[sock.id].owns];
					delete users[sock.id];
					sizeUsers = _.size(users);
					sizeRooms = _.size(rooms);
					io.sockets.in(sock.room).emit('updateUsers', {users: users});
					io.sockets.emit('updateRooms', rooms);
				}else if(action === "removeRoom") {
					io.sockets.in(sock.room).emit("notif", "The owner has removed the room");
					var socketIds = [];
					for (var i=0; i<sockets.length; i++){
						socketIds.push(sockets[i].id);
						if(_.contains((socketIds)), room.users){
							sockets[i].leave(room.name);
						}
					}

					if(_.contains((room.users)), sock.id){
						for (var i=0; i< room.users.length; i++){
							users[room.users[i]].inRoom = null;
						}
					}
					delete rooms[users[sock.id].owns];
					users[sock.id].owns = null;;
					room.users = _.without(room.users, sock.id);
					delete chatHistory[room.name];
					io.sockets.emit('updateRooms', rooms);
				} else if(action === "leaveRoom"){
						io.sockets.in(sock.room).emit("notif", "The owner has left the room");
						var socketIds = [];
						for (var i=0; i<sockets.length; i++){
							socketIds.push(sockets[i].id);
							if(_.contains((socketIds)), room.users){
								sockets[i].leave(room.name);
							}
						}

						if(_.contains((room.users)), sock.id){
							for (var i=0; i< room.users.length; i++){
								users[room.users[i]].inRoom = null;
							}
						}
						delete rooms[users[sock.id].owns];
						users[sock.id].owns = null;;
						room.users = _.without(room.users, sock.id);		
						delete chatHistory[room.name];
						io.sockets.emit('updateRooms', rooms);
				}

			} else {
				if(action === 'disconnect'){
					io.sockets.emit("notif", users[sock.id].name + " has disconnected from the server.");
					if (_.contains((room.users), sock.id)){
						var userIndex = room.users.indexOf(sock.id);
						room.users.splice(userIndex, 1);
						sock.leave(room.name);
					}
					delete users[sock.id];
					io.sockets.in(sock.room).emit("updateUsers", {users : users});
					var o = _.findWhere(sockets, {'id': sock.id});
					sockets = _.without(sockets, o);

				}else if(action === 'removeRoom'){
					sock.emit("notif", "Only the owner can remove a room.");
				} else if(action === 'leaveRoom'){
					if(_.contains((room.users)), sock.id){
						var userIndex = room.users.indexOf(sock.id);
						room.users.splice(userIndex, 1);
						users[sock.id].inRoom = null;
						io.sockets.emit("notif", users[sock.id].name + " has left the room.");
						sock.leave(room.name);
						var usersToEmit = {};
						for(var key in users){
							if(users[key].inRoom !== null)
								usersToEmit[key] = users[key];
						}
        		io.sockets.in(sock.room).emit("updateUsers", {users: usersToEmit});
					}
				}
			}
		} else {
      if (action === "disconnect") {
        io.sockets.emit("update", users[sock.id].name + " has disconnected from the server.");
        delete users[sock.id];
        sizePeople = _.size(users);
        io.sockets.in(sock.room).emit("updateUsers", {users: users});
        var o = _.findWhere(sockets, {'id': sock.id});
        sockets = _.without(sockets, o);
      }     
		}
	}

  io.sockets.on('connection', function (socket) {
		console.log('Nouveau utilisateur');

		/*for(var i in users){
			socket.emit('updateUsers', users[i])
		}*/

		socket.on('joinServer', function(data){
			var ownerRoomId = inRoomId = null;
      if(data.username !== ""){
      	users[socket.id] = {"name": data.username, "owns": ownerRoomId, "inRoom": inRoomId};
      	socket.emit("notif", "You are connected to the server");
      	io.sockets.emit("notif", users[socket.id].name + " is online");
				io.sockets.emit("updateRooms", rooms);
				sockets.push(socket);	      	
				console.log(users[socket.id]);
      }
		});

		socket.on('disconnect', function(){
			if(typeof users[socket.id] !== 'undefined'){
				game(socket, "disconnect");
			}
		});

		socket.on('chat', function(message){
			if(io.sockets.manager.roomClients[socket.id]['/'+socket.room] !== undefined){
				date = new Date();
				var objMessage = {message: message, h: date.getHours(), m: date.getMinutes()};
				io.sockets.in(socket.room).emit('chat', {user : users[socket.id], message: objMessage});
				if(_.size(chatHistory[socket.room]) > 10){
					chatHistory[socket.room].splice(0,1);
				}else{
					chatHistory[socket.room].push(users[socket.id].name + ": "+message)
				}
			} else {
				socket.emit("notif", "Please connect to a room");
			}
		});

		socket.on("createRoom", function(ro) { 
			if(!users[socket.id].owns){
				var id = _.size(rooms) + 1;
				var room = new Room(ro.name, id, ro.language, socket.id);
				rooms[id] = room;
				io.sockets.emit("updateRooms", rooms);

				socket.room = ro.name;
				socket.join(socket.room);
				users[socket.id].owns = id;
				users[socket.id].inRoom = id;
				room.addUser(socket.id);
				socket.emit("notif", "Welcome to "+room.name+".");
				socket.emit("gameStart", room);
      	socket.emit("updateUsers", {users: users});
				chatHistory[socket.room] = [];
			}else{
				socket.emit("notif", "You have already created a room.");
			}
		});
		
		socket.on("joinRoom", function(id){
			var room = rooms[id];
			if (socket.id === room.owner){
				socket.emit("notif", "You are the owner and you already joined");
			} else{
				if(_.contains((room.users), socket.id)) {
					socket.emit("notif", "You are already joined this room");
				} else {
					if (users[socket.id].inRoom !== null){
						socket.emit("notif", "You are already in a room");
					} else {
						room.addUser(socket.id);
						users[socket.id].inRoom = id;
						socket.room = room.name;
						socket.join(socket.room);
						user = users[socket.id];
						io.sockets.in(socket.room).emit("notif", user.name+ " has connected to the room");
    				io.sockets.in(socket.room).emit("updateUsers", {users: users});
						socket.emit("notif", "Welcome to "+room.name+".");
						socket.emit("gameStart", room);
						var keys = _.keys(chatHistory);
						if (_.contains(keys, socket.room)){
							socket.emit("history", chatHistory[socket.room]);
						}
						room.lines.forEach(function (line) {
                socket.emit('draw:line', line);
        		});
					}
				}
			}
		});

		socket.on("checkName", function(name, fn){
			var match = false;
			_.find(rooms, function(key, value) {
				if(key.name === name)
					return match= true;
			});
			fn({result: match});
		})

		socket.on("removeRoom", function(id) {
			var room = rooms[id];
			if (socket.id === room.owner){
				game(socket, "removeRoom");
			} else {
				socket.emit("notif", "Only the owner can remove a room.");
			}
		});



		socket.on("leaveRoom", function(id){
			var room = rooms[id];
			socket.emit('draw:clear');
			if(room)
				game(socket, "leaveRoom");
		});


		socket.on('draw:line', function(line, id){
			var room = rooms[id];
			room.addLines(line);
			socket.broadcast.to(socket.room).emit('draw:line', line);
		});

		socket.on('draw:clear', function(id) {
			var room = rooms[id];
			room.clearLines();
			socket.broadcast.to(socket.room).emit('draw:clear');
		})


		//console.log(rooms);
	//  socket.emit('rooms', JSON.stringify(rooms));

	  /*setInterval(function () {
	    socket.emit('send:time', {
	      time: (new Date()).toString()
	    });
	  }, 1000);*/

  });

}