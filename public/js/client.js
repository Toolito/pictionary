$(function(){

	// Socket.io
	socket = io.connect();
	var myRoomID = null;

	$('html').one('mouseover', function(){
		if(window.location.hash == "#connect"){
			socket.emit('joinServer', {
				username : $('#username').text() 
			});
		}
	});

	$('#rooms').on('click', '.joinBtn', function() {
		var roomName = $(this).attr("rel");
		var roomId = $(this).attr("id");
		socket.emit("joinRoom", roomId);
	});

	$('#createRoomBtn').click(function() {
		var roomExists = false;
		var room = {};
		room.name = $('#createRoomName').val();
		room.language = $('select#createRoomLg option:selected').val();
		socket.emit('checkName', room.name, function(data){
			roomExists = data.result;
			if(roomExists){
				noty({text: "Room already exists", layout: "bottom", timeout: 2000});
			}else {
				if(room.name.length > 0){
					socket.emit("createRoom", room);
				}
			}
		})
	});

	$("#leaveRoom").click(function() {
    var roomID = myRoomID;
    alert('my room id : ' +roomID);
    socket.emit("leaveRoom", roomID);
    $("#game").hide();
    $('#lobby').show();
  });

	var now = new Date();
	now.setMinutes(now.getMinutes() + 3);

	$('#formMsg').submit(function(e){
		e.preventDefault();
		socket.emit('chat', $('#message').val())
		$('#message').val('');
		$('#message').focus();
	});

 	socket.on("gameStart", function(room) {
 	  myRoomID = room.id;
 	  $('#roomName').empty().append('['+room.language+'] '+room.name);
 	  $('#roomName').attr('rel', myRoomID);
 	  $('#game').show();
 	  $('#lobby').hide();
 	  $('.modal-dialog').modal('hide');
 	  $('.modal-backdrop').remove();
 	  Draw.init(socket);
  });

	socket.on('chat', function(data){
		$('#messages').append('<div class="listAsc"><span class="col-xs-2">'+data.user.name+'</span><span class="col-xs-8">'+data.message.message+'</span><span class="col-xs-2"><small>'+data.message.h+':'+data.message.m+'<small></span>');
	});

	$('.colorpicker').colorpicker();


	socket.on('updateRooms', function(data){
		$('#rooms').text('');
		var html = '';
		$.each(data, function(id, room){
			html += '<tr><td>['+room.language+'] '+room.name+'</td>';
			html += '<td> /4</td>';
			html += '<td><a class="joinBtn btn btn-mini btn-success" id="'+room.id+'" rel="'+room.name+'" >Join</a></td></tr>';
		});
		$('#rooms').append(html);
	})

	socket.on('updateUsers', function(data){
		$('#users').empty();
		$.each(data.users, function(i, obj){
			$('#users').append('<li id="' + obj.id +'">'+obj.name+'</li>');
		});
	});

	socket.on('notif', function(message){
		noty({text: message, layout: "bottom", timeout: 2000});
	});

	socket.on('discoUser', function(user){
		$('#'+user._id).remove();
	})

});
