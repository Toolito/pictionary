var Draw = function(){

	var drawInit = function(socket){

		var canvas = null,
  	ctx = null,
  	firstClick = true,
  	mouseDown = false,
  	x1, x2 = 0,
  	y1, y2 = 0,
  	clearBtn = null;

		var roomId = $('#roomName').attr('rel');

		// Canvas init
		canvas = document.getElementById("draw-div");
		canvasHeight = canvas.offsetHeight;
		canvasWidth = canvas.offsetWidth;
		ctx = canvas.getContext("2d");

		// Canvas listeners
		canvas.addEventListener('mousedown', function(e){
			e.preventDefault();
			mouseDown = true;
		});

		canvas.addEventListener('mouseup', function(e) {
			e.preventDefault();
			mouseDown = false;
			firstClick = true;
		});

		canvas.addEventListener('mousemove', function(e){
			e.preventDefault();
			var rect = canvas.getBoundingClientRect();
			if(mouseDown){
				handleUserDrawAtPosition(e.clientX - rect.left, e.clientY - rect.top);
			}
		});

		socket.on('draw:line', function(line){
			drawLine(line, "rgb(0, 0, 0)");
		});

		socket.on('draw:clear', function(){
			clearCtx();
		});

		function handleUserDrawAtPosition(x, y){

			x1 = x2;
			y1 = y2;
			x2 = x;
			y2 = y;

			if(firstClick){
				x1 = x2;
				y1 = y2;
				x2++;
				y2++;
				firstClick = false;
			}

			var line = new Line([x1, y1], [x2, y2]);
			drawLine(line, "rgb(0, 0, 0)");

			socket.emit('draw:line', line, roomId);
		}

		function drawLine(line, color)
		{
			ctx.save();
			ctx.beginPath();
			ctx.strokeStyle = color;
			ctx.moveTo(line.from.x, line.from.y);
			ctx.lineTo(line.to.x, line.to.y);
			ctx.closePath();
			ctx.stroke();
			ctx.restore();
		}

		clearBtn = document.getElementById("clearBtn");

		clearBtn.addEventListener('click', function (e){
			clearCtx();
			socket.emit('draw:clear', roomId);
		});

		var clearCtx = function(){
			firstClick = true;
			ctx.clearRect(0, 0, canvasWidth, canvasHeight);	
		};


		function Line(from, to)
		{
			this.from = new Coordinate(from[0], from[1]);
			this.to = new Coordinate(to[0], to[1]);
		}

		function Coordinate(x, y){
			this.x = x;
			this.y = y;
		}
	}

	var clearBoard = function() {
		var canvas = document.getElementById("draw-div");
		canvasHeight = canvas.offsetHeight;
		canvasWidth = canvas.offsetWidth;
  	ctx = canvas.getContext("2d");
		ctx.clearRect(0, 0, canvasWidth, canvasHeight);	
	}

	return {
		init: function(socket){
			drawInit(socket);
		},
		clearBoard: function() {
			clearBoard();
		}
	};
  

}();