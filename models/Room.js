
//Room object
function Room(name, id, language, owner){
    this.name = name;
    this.id = id;
    this.language = language;
    this.owner = owner;
    this.users = [];
    this.usersLimit = 4;
    this.status = "open";
    this.timer = 180;
    this.lines = [];
}

Room.prototype.addUser = function(userID){
    if (this.status === 'open'){
        this.users.push(userID);
    }
};

Room.prototype.removeUser = function(user){
    var userIndex = -1;
    for(var i = 0; i < this.users.length; i++){
        if(this.users[i].id === user.id){
            userIndex = i;
            break;
        }
    }
    this.users.remove(userIndex);
};

Room.prototype.isOpen = function() {
    if(this.status === 'open'){
        return true;
    } else {
        return false;
    }
};

Room.prototype.getLines = function() {
    return this.lines;
};

Room.prototype.addLines = function(lines) {
    this.lines.push(lines);
};

Room.prototype.clearLines = function() {
    this.lines = [];
};



exports.RoomObj = Room;