var mongoose   = require('./getMongoose.js').mongoose,
    UserSchema = mongoose.Schema({
        username: {
            type: String,
            required: true,
            unique: true
        },
        email: {
            type: String,
            required: true,
            unique: true
        },
        password: {
            type: String,
            required: true
        },
        wins: {
            type: Number
        },
        looses: {
            type: Number
        }
    }),
    UserModel = mongoose.model('User', UserSchema);
exports.User = UserModel;


exports.findById = function(req, res) {
    var id = req.params.id;
    console.log('Retrieving user: ' + id);
    UserModel.findById(id, function(e,r){
        if(r != null){
            res.send(r);
        }
        else{
            console.log('User not found');
            res.send(null);
        }
    })
};

exports.findAll = function(req, res) {
    UserModel.find(function (err, users){
        if(err) {res.send({'error':'An error has occured'}); }
        res.send(users);
    });
};

exports.addUser = function(req, res, callback) {
    var user = new UserModel(req.body);
    console.log('Adding user!');
    user.save( function(err) {
        if(err) {console.log(err);}
        console.log('saved');
    });
    res.send('item saved');
}

exports.updateUser = function(req, res, callback) {
    var id = req.params.id;
    var user = req.body;
    UserModel.findOne({'_id':id}, function(e,r){
        if(r != null){
            UserModel.update({'_id':id}, user, function(err){
                if(err) {console.log(err);}
                res.send('item updated');
            });
        }
        else{
            res.send('User not found');
        }
    });
}

exports.deleteUser = function(req, res, callback) {
    var id = req.params.id;
    UserModel.remove({'_id':id}, function(err, result){
        if(err){
            res.send({'error':'An error has occured - '+ err});
        } else {
            console.log(result + 'document(s) deleted');
            res.send(result + 'document(s) deleted');
        }
    })
}

exports.login = function(req, res, callback) {
    var email = req.body.email;
    var pass = req.body.password;
    UserModel.findOne({'email':email}, function(e,r){
        if(r != null){
            if(r.password == pass){
                console.log('Auth OK');
                res.send(r);
            } else {
                console.log('Auth err');
                res.send(null);
            }
        }
        else
            res.send(null)
    })
}

// Private encryption & validation methods
/*var generateSalt = function()
 {
 var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
 var salt = '';
 for (var i = 0; i < 10; i++) {
 var p = Math.floor(Math.random() * set.length);
 salt += set[p];
 }
 return salt;
 }

 var md5 = function(str) {
 return crypto.createHash('md5').update(str).digest('hex');
 }

 var saltAndHash = function(pass, callback)
 {
 var salt = generateSalt();
 callback(salt + md5(pass + salt));
 }*/

/*var validatePassword = function(plainPass, hashedPass, callback)
 {
 var salt = hashedPass.substr(0, 10);
 var validHash = salt + md5(plainPass + salt);
 callback(null, hashedPass === validHash);
 }*/

var populateDB = function() {

    var users = [
        {
            email: "pierre@schrepel.me",
            password: "admin",
            username: "admin",
        }
    ];

    db.collection('users', function(err, collection) {
        collection.insert(users, {safe:true}, function(err, result) {});
    })
}