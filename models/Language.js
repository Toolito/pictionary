var mongoose   = require('./getMongoose.js').mongoose,
	LanguageSchema = mongoose.Schema({
		name: {
			type: String,
			required: true,
			unique: true
		},
		abv: {
			type: String,
		},
	}),
	LanguageModel = mongoose.model('Language', LanguageSchema);
exports.Language = LanguageModel;

LanguageModel.count(null, function(err, c){
	if(c != 72){
		console.log(c);
		populateDB();
	}
});


var populateDB = function() {

	var languages = [
		{name:'Afrikaans',abv:'AF'},
		{name:'Albanian',abv:'SQ'},
		{name:'Arabic',abv:'AR'},
		{name:'Armenian',abv:'HY'},
		{name:'Basque',abv:'EU'},
		{name:'Bengali',abv:'BN'},
		{name:'Bulgarian',abv:'BG'},
		{name:'Catalan',abv:'CA'},
		{name:'Cambodian',abv:'KM'},
		{name:'Chinese(Mandarin)',abv:'ZH'},
		{name:'Croatian',abv:'HR'},
		{name:'Czech',abv:'CS'},
		{name:'Danish',abv:'DA'},
		{name:'Dutch',abv:'NL'},
		{name:'English',abv:'EN'},
		{name:'Estonian',abv:'ET'},
		{name:'Fiji',abv:'FJ'},
		{name:'Finnish',abv:'FI'},
		{name:'French',abv:'FR'},
		{name:'Georgian',abv:'KA'},
		{name:'German',abv:'DE'},
		{name:'Greek',abv:'EL'},
		{name:'Gujarati',abv:'GU'},
		{name:'Hebrew',abv:'HE'},
		{name:'Hindi',abv:'HI'},
		{name:'Hungarian',abv:'HU'},
		{name:'Icelandic',abv:'IS'},
		{name:'Indonesian',abv:'ID'},
		{name:'Irish',abv:'GA'},
		{name:'Italian',abv:'IT'},
		{name:'Japanese',abv:'JA'},
		{name:'Javanese',abv:'JW'},
		{name:'Korean',abv:'KO'},
		{name:'Latin',abv:'LA'},
		{name:'Latvian',abv:'LV'},
		{name:'Lithuanian',abv:'LT'},
		{name:'Macedonian',abv:'MK'},
		{name:'Malay',abv:'MS'},
		{name:'Malayalam',abv:'ML'},
		{name:'Maltese',abv:'MT'},
		{name:'Maori',abv:'MI'},
		{name:'Marathi',abv:'MR'},
		{name:'Mongolian',abv:'MN'},
		{name:'Nepali',abv:'NE'},
		{name:'Norwegian',abv:'NO'},
		{name:'Persian',abv:'FA'},
		{name:'Polish',abv:'PL'},
		{name:'Portuguese',abv:'PT'},
		{name:'Punjabi',abv:'PA'},
		{name:'Quechua',abv:'QU'},
		{name:'Romanian',abv:'RO'},
		{name:'Russian',abv:'RU'},
		{name:'Samoan',abv:'SM'},
		{name:'Serbian',abv:'SR'},
		{name:'Slovak',abv:'SK'},
		{name:'Slovenian',abv:'SL'},
		{name:'Spanish',abv:'ES'},
		{name:'Swahili',abv:'SW'},
		{name:'Swedish',abv:'SV'},
		{name:'Tamil',abv:'TA'},
		{name:'Tatar',abv:'TT'},
		{name:'Telugu',abv:'TE'},
		{name:'Thai',abv:'TH'},
		{name:'Tibetan',abv:'BO'},
		{name:'Tonga',abv:'TO'},
		{name:'Turkish',abv:'TR'},
		{name:'Ukrainian',abv:'UK'},
		{name:'Urdu',abv:'UR'},
		{name:'Uzbek',abv:'UZ'},
		{name:'Vietnamese',abv:'VI'},
		{name:'Welsh',abv:'CY'},
		{name:'Xhosa',abv:'XH'}
	];

	LanguageModel.collection.insert(languages, {safe:true}, function(err, result) {});
}